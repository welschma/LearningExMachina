# ---- Import from root_numpy library 
import root_numpy
from root_numpy import root2array, rec2array

# ---- Import common python libraries
import sys
import time
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import random

# ---- Import from root_pandas library
import root_pandas
from root_pandas import read_root

# ---- Import panda library
from pandas.tools import plotting
from pandas.tools.plotting import scatter_matrix
from pandas.core.index import Index
import pandas.core.common as com

# ---- Import scipy
import scipy
from scipy.stats import ks_2samp
import scipy as sp

# ----Import itertools
import itertools
from itertools import cycle

# Import Jupyter
from IPython.core.interactiveshell import InteractiveShell

# ---- Keras deep neural network library
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.optimizers import SGD
from keras.wrappers.scikit_learn import KerasClassifier
from keras.regularizers import l1, l2 #,WeightRegularizer
from keras.constraints import maxnorm
from keras.models import model_from_json

# ----- Import scikit-learn
import sklearn
from sklearn.feature_selection import VarianceThreshold
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.feature_selection import RFE
from sklearn.feature_selection import SelectFromModel

from sklearn.model_selection import ParameterGrid
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import KFold, StratifiedKFold, ShuffleSplit
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import learning_curve

from sklearn.svm import SVC, LinearSVC
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier, VotingClassifier
from sklearn.ensemble import GradientBoostingClassifier, AdaBoostClassifier, BaggingClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import RandomizedLasso

from sklearn.preprocessing import StandardScaler, RobustScaler, MinMaxScaler
from sklearn.feature_selection import SelectPercentile, f_classif
from sklearn.feature_selection import RFECV
import sklearn.svm
from sklearn.calibration import calibration_curve, CalibratedClassifierCV
from sklearn.pipeline import make_pipeline, Pipeline
from sklearn.metrics import (confusion_matrix, roc_auc_score, roc_curve, 
                             auc, average_precision_score, precision_score, 
                             brier_score_loss, recall_score, f1_score, log_loss, 
                             classification_report, precision_recall_curve, accuracy_score)

from sklearn.dummy import DummyClassifier

from sklearn.externals import joblib
from sklearn import feature_selection

# ---- Import imblearn
import imblearn
from imblearn.over_sampling import ADASYN, SMOTE, RandomOverSampler
from imblearn.under_sampling import RandomUnderSampler


# ----- Python utilities
from collections import defaultdict, Counter

# Python regular-expression
import re

# ---- Sciki-kit learn graph 
from sklearn.tree import export_graphviz

# ---- Check the versions of libraries/packages
print("Python version " + sys.version)
print("Sklearn version " + sklearn.__version__)
print("Root_numpy version " + root_numpy.__version__)
print("Numpy version " + np.__version__)
print("Scipy version " + scipy.__version__)
print("Pandas version " + pd.__version__)
print("Matplotlib version " + matplotlib.__version__)
print("Seaborn version " + sns.__version__)
print("Imblance version " +imblearn.__version__)

# ---- Fix random seed for reproducibility
seed = 7
np.random.seed(seed)

# ---- Data loading function
def load(sig_filename, bkg_filename, category, features):
    """Data loader.
    
    Parameters
    ----------
    sig_filename : array, shape = [n_samples]
            true class, intergers in [0, n_classes - 1)
    bkg_filename : array, shape = [n_samples, n_classes]
    category: string
    features: array, shape = [n_features]

    Returns
    -------
    data : pandas.DataFrame
    """
   
    signal = read_root([sig_filename], category, columns=features)
    background = read_root([bkg_filename], category, columns=features)
    
    signal['y'] = 1.
    background['y'] = 0.
      
    # for sklearn data is usually organised
    # into one 2D array of shape (n_samples x n_features)
    # containing all the data and one array of categories
    # of length n_samples
    data = pd.concat([signal, background])

    return data


# ---- Feature names
branch_names = """mass_tag_tag_min_deltaR,median_mass_jet_jet,
    maxDeltaEta_tag_tag,mass_higgsLikeDijet,HT_tags,
    btagDiscriminatorAverage_tagged,mass_jet_tag_min_deltaR,
    mass_jet_jet_min_deltaR,mass_tag_tag_max_mass,maxDeltaEta_jet_jet,
    centrality_jets_leps,centrality_tags,globalTimesEventWeight""".split(",")

features = [c.strip() for c in branch_names]
features = (b.replace(" ", "_") for b in features)
features = list(b.replace("-", "_") for b in features)

# --- Load dataset
signal_sample = "../combined/signalMC.root"
background_sample = "../combined/backgroundMC.root"
tree_category = "event_mvaVariables_step7_cate4"

data = load(signal_sample, background_sample, tree_category, features)

print "Total number of events: {}\nNumber of features: {}".format(data.shape[0], data.shape[1])

# ---- Store a copy for later use
df_archived = data.copy(deep=True)


# ---- Print statistical summary of dataset
print "Head:"
data.head()
print "Information:" 
data.info()
print "Describe:"
data.describe()


# ---- Create features dataframe and target array
data_X = data.drop(["y","globalTimesEventWeight"], axis=1, inplace=False)
data_y = data["y"]
input_dim = data_X.shape[1]
print("[input_dim]: ",input_dim)
## Create network with Keras

# Function to create model, required for KerasClassifier
def create_model_tune_batch_epoch():
    # create model
    model = Sequential()
    model.add(Dense(12, input_dim=input_dim, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))
    # Compile model
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model

def create_model_tune_optimizer_algo(optimizer='adam'):
    # create model
    model = Sequential()
    model.add(Dense(12, input_dim=input_dim, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))
    # Compile model
    model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])
    return model

def create_model_tune_learn_momentum(learn_rate=0.01, momentum=0):
    # create model
    model = Sequential()
    model.add(Dense(12, input_dim=input_dim, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))
    # Compile model
    optimizer = SGD(lr=learn_rate, momentum=momentum)
    model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])
    return model

def create_model_tune_weight_init(init_mode='uniform'):
    # create model
    model = Sequential()
    model.add(Dense(12, input_dim=input_dim, init=init_mode, activation='relu'))
    model.add(Dense(1, init=init_mode, activation='sigmoid'))
    # Compile model
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model

def create_model_tune_neuron_activation(activation='relu'):
    # create model
    model = Sequential()
    model.add(Dense(12, input_dim=input_dim, init='uniform', activation=activation))
    model.add(Dense(1, init='uniform', activation='sigmoid'))
    # Compile model
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model

def create_model_tune_regularization(dropout_rate=0.0, weight_constraint=0):
    # create model
    model = Sequential()
    #model.add(Dense(12, input_dim=input_dim, init='uniform', activation='linear', kernel_constraint=maxnorm(weight_constraint)))
    model.add(Dense(12, input_dim=input_dim, init='uniform', activation='linear'))
    model.add(Dropout(dropout_rate))
    model.add(Dense(1, init='uniform', activation='sigmoid'))
    # Compile model
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model

def create_model_tune_number_nuerons(neurons=1):
    # create model
    model = Sequential()
    #model.add(Dense(neurons, input_dim=input_dim, init='uniform', activation='linear', kernel_constraint=maxnorm(4.)))
    model.add(Dense(neurons, input_dim=input_dim, init='uniform', activation='linear'))
    model.add(Dropout(0.2))
    model.add(Dense(1, init='uniform', activation='sigmoid'))
    # Compile model
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model


# preprocessing using 0-1 scaling byremoving the mean and scaling to unit variance 
scaler = RobustScaler()

# ---- Create model for use in scikit-learn
pipe_classifiers = {
    'kdnn_tune_batch_epoch':        make_pipeline(scaler,  KerasClassifier(build_fn=create_model_tune_batch_epoch,    verbose=0)),
    'Kdnn_tune_optimizer_algo':     make_pipeline(scaler,  KerasClassifier(build_fn=create_model_tune_optimizer_algo, verbose=0)),
    'kdnn_tune_learn_momentum':     make_pipeline(scaler,  KerasClassifier(build_fn=create_model_tune_learn_momentum, verbose=0)),
    'kdd_tune_weight_init':         make_pipeline(scaler,  KerasClassifier(build_fn=create_model_tune_weight_init,    verbose=0)),
    'kdnn_tune_neuron_activation':  make_pipeline(scaler,  KerasClassifier(build_fn=create_model_tune_neuron_activation, verbose=0)),
    'kdnn_tune_regularization':     make_pipeline(scaler,  KerasClassifier(build_fn=create_model_tune_regularization, verbose=0)),
    'kdnn_tune_number_nuerons':     make_pipeline(scaler,  KerasClassifier(build_fn=create_model_tune_number_nuerons, verbose=0))
}

## Tune Batch Size and Number of Epochs

# ---- Define the grid search parameters
batch_size = [10, 20, 40]
epochs = [10, 50]
#param_grid = dict(batch_size=batch_size, nb_epoch=epochs)
param_grid = dict(**{'kerasclassifier__nb_epoch':batch_size,
                     'kerasclassifier__batch_size': epochs
                 })

# ---- Run grid search with cross-validation
grid = GridSearchCV(estimator=pipe_classifiers['kdnn_tune_batch_epoch'], param_grid=param_grid, cv=3, n_jobs=-1)
#grid = GridSearchCV(estimator= KerasClassifier(build_fn=create_model_tune_batch_epoch), param_grid=param_grid, cv=3, n_jobs=-1)
grid_result = grid.fit(data_X.values, data_y.values)

# ---- Summarize results
print("\nBest: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))



## Tune optimization algorithms

# ---- Define the grid search parameters
optimizer = ['SGD', 'RMSprop', 'Adagrad', 'Adadelta', 'Adam']
param_grid = dict(kerasclassifier__optimizer=optimizer)

# ---- Run grid search with cross-validation
grid = GridSearchCV(estimator=pipe_classifiers['Kdnn_tune_optimizer_algo'], param_grid=param_grid, cv=3, n_jobs=-1)
grid_result = grid.fit(data_X.values, data_y.values)

# ---- Summarize results
print("\nBest: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))



## Tune learning rate and momentum

# ---- Define the grid search parameters
learn_rate = [0.001, 0.01, 0.1, 0.2, 0.3]
momentum = [0.0, 0.2, 0.4, 0.6, 0.8, 0.9]
param_grid = dict(kerasclassifier__learn_rate=learn_rate, kerasclassifier__momentum=momentum)

# ---- Run grid search with cross-validation
grid = GridSearchCV(estimator=pipe_classifiers['kdnn_tune_learn_momentum'], param_grid=param_grid, cv=3, n_jobs=-1)
grid_result = grid.fit(data_X.values, data_y.values)

# ---- Summarize results
print("\nBest: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))



## Tune Network Weight Initialization

# ---- Define the grid search parameters
init_mode = ['uniform', 'lecun_uniform', 'normal', 'zero', 'glorot_normal', 'glorot_uniform']
param_grid = dict(kerasclassifier__init_mode=init_mode)

# ---- Run grid search with cross-validation
grid = GridSearchCV(estimator=pipe_classifiers['kdd_tune_weight_init'], param_grid=param_grid, cv=3, n_jobs=-1)
grid_result = grid.fit(data_X.values, data_y.values)

# ---- Summarize results
print("\nBest: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))



## Tune the Neuron Activation Function

# ---- Define the grid search parameters
activation = ['softmax', 'softplus', 'softsign', 'relu', 'tanh', 'sigmoid', 'hard_sigmoid']
param_grid = dict(kerasclassifier__activation=activation)

# ---- Run grid search with cross-validation
grid = GridSearchCV(estimator=pipe_classifiers['kdnn_tune_neuron_activation'], param_grid=param_grid, cv=3, n_jobs=-1)
grid_result = grid.fit(data_X.values, data_y.values)

# ---- summarize results
print("\bBest: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))



## Tune Dropout Regularization

# ---- Define the grid search parameters
weight_constraint = [1, 2, 3, 4, 5]
dropout_rate = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
param_grid = dict(kerasclassifier__dropout_rate=dropout_rate, kerasclassifier__weight_constraint=weight_constraint)

# ---- Run grid search with cross-validation
grid = GridSearchCV(estimator=pipe_classifiers['kdnn_tune_regularization'], param_grid=param_grid, cv=3, n_jobs=-1)
grid_result = grid.fit(data_X.values, data_y.values)

# ---- Summarize results
print("\nBest: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))



## Tune the Number of Neurons in the Hidden Layer

# ----- Define the grid search parameters
neurons = [5, 10, 15, 20, 25, 30]
param_grid = dict(kerasclassifier__neurons=neurons)

# ---- Run grid search with cross-validation
grid = GridSearchCV(estimator=pipe_classifiers['kdnn_tune_number_nuerons'], param_grid=param_grid, cv=3, n_jobs=-1)
grid_result = grid.fit(data_X.values, data_y.values)

# ---- Summarize results
print("\nBest: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))

## Visualize weights: Heat Map of neural network weights

# Heat map of the first layer weights in a neural network learned on the HEP dataset

# ---- Get the neural network weights
def dnn_weight_map(classifier):
    
    #W,b = pcv.best_estimator_.named_steps['classifier'].model.layers[0].get_weights()
    W,b = classifier.model.layers[0].get_weights()

    W = np.squeeze(W)
    print("W shape : ", W.shape)

    _= plt.figure(figsize=(10, 8))
    _= plt.imshow(W, interpolation='nearest', cmap='viridis')

    # Heat map
    _= plt.yticks(range(12), features)
    _= plt.xlabel("Columns in weight matrix")
    _= plt.ylabel("Input feature")
    _= plt.colorbar()
    _= plt.grid("off")
    
    return plt.show()

# ---- Visualize weights: Heat Map of neural network weights
#pipe_classifiers["Kdnn_tune_optimizer_algo"].fit(data_X, data_y)
#dnn_weight_map(pipe_classifiers["Kdnn_tune_optimizer_algo"].named_steps['kerasclassifier'])


# ---- Compute ROC curve and area under the curve
def roc_plot(models, X, y):
    """Roc curve metric plotter.

    Parameters
    ----------
    models : dictionary, shape = [n_models]
    X : DataFrame, shape = [n_samples, n_classes]
    y : DataFrame, shape = [n_classes]

    Returns
    -------
    roc : matplotlib plot
    """
     
    # Split data into a development and evaluation set
    X_dev,X_eval, y_dev,y_eval = train_test_split(X, y, test_size=0.33, random_state=42)
    # Split development set into a train and test set
    X_train, X_test, y_train, y_test = train_test_split(X_dev, y_dev, 
                                                        test_size=0.33, random_state=seed)
    
    # contains rates for ML classifiers
    fpr = {}
    tpr = {}
    roc_auc = {}
    
    # Customize the major grid
    fig, ax = plt.subplots()
    ax.grid(which='major', linestyle='-', linewidth='0.2', color='gray')
    ax.patch.set_facecolor('white')
    
    # Include random by chance 'luck' curve
    plt.plot([1, 0], [0, 1], '--', color=(0.1, 0.1, 0.1), label='Luck')
        
    # Loop through classifiers
    for (name, model) in models.items():
        
        print "\n\x1b[1;31mBuilding model ...\x1b[0m"
        process = time.clock()
        model.fit(X_train, y_train)

        print "\t%s fit time: %.3f"%(name, time.clock()-process)
        
        y_predicted = model.predict(X_test)
        
        if hasattr(model, "predict_proba"):
            decisions = model.predict_proba(X_test)[:, 1]
        else:  # use decision function
            decisions = model.decision_function(X_test)
        
        print "\tArea under ROC curve for %s: %.4f"%(name, roc_auc_score(y_test,decisions))
        
        process = time.clock()
        #print classification_report(y_test, y_predicted, target_names=['signal', 'background'])
        print("\tScore of test dataset: {:.5f}".format(model.score(X_test, y_test)))

        process = time.clock()
        fpr[name], tpr[name], thresholds = roc_curve(y_test, decisions)
        print "\tArea under ROC time: ", time.clock()-process
        
        roc_auc[name] = auc(fpr[name], tpr[name])
    
    colors = cycle(['aqua', 'darkorange', 'cornflowerblue', 
                    'green', 'yellow', 'SlateBlue', 'DarkSlateGrey',
                    'CadetBlue', 'Chocolate', 'darkred', 'GoldenRod'])
  
    for (name, model), color in zip(models.items(), colors):

        plt.plot(tpr[name], 1-fpr[name], color=color, lw=2,
                 label='%s (AUC = %0.3f)'%(name, roc_auc[name]))                 
    
    # Plot all ROC curves
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title("Receiver operating characteristic ({} events)".format(X.shape[0]))
    leg = plt.legend(loc="lower left", frameon=True, fancybox=True, fontsize=8) # loc='best'
    leg.get_frame().set_edgecolor('w')
    frame = leg.get_frame()
    frame.set_facecolor('White')
    
    return plt.show()


# ---- Assessing a Classifier's Performance
#roc_plot(pipe_classifiers, data_X, data_y)

# ---- Creat model
def create_model(optimizer='rmsprop', init='glorot_uniform', dropout_rate=0.0):
    """Define keras deep learning model.

    Parameters
    ----------
    optimizer : <TBF>  
    init : <TBF>  
    dropout_rate : <TBF>

    Returns
    -------
    model : <TBF>  
    """
    
    # create model: create a simple multi-layer neural network for the problem.

    # Note: initialization of the weights was chose as default to be 
    # randomly drawn from a uniform distribution (if normal then the distribution
    # would have mean 0 and standard deviation 0.05 in keras)

    # expected input data shape: (batch_size, timesteps, data_dim)
    model = Sequential()
    # Rectify Linear Unit (Relu) = relu, Exponential Linear Unit (Elu) =  elu
    model.add(Dense(12, input_dim=12, init=init, activation='relu')) 
    # ReLu(x) = {0 for x <=0 else x for x > 0}
    model.add(Dropout(dropout_rate))
     # 8 neurons in the hidden layer and 12 in the visible layer 
    model.add(Dense(8, init=init, activation='relu')) # 8 neurons in the hidden layer 
    model.add(Dropout(dropout_rate))
    model.add(Dense(1, init=init, activation='sigmoid')) # 1 neuron in the output layer

    # Compile model
    model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])
    
    return model

# ---- Split dataset
X_train, X_test, y_train, y_test = train_test_split(data_X.values, data_y.values, test_size=0.33, random_state=seed)

# ---- Default pipeline setup with dummy place holder steps
pipe = Pipeline([('feature_scaling', None), 
                 ('feature_selection', None), 
                 ('classifier', DummyClassifier())])

# ---- preprocessing using 0-1 scaling byremoving the mean and scaling to unit variance 
scaler = RobustScaler()

# ---- feature selection
select = SelectKBest(k=4)

# ---- create classifier for use in scikit-learn
model = KerasClassifier(build_fn=create_model, nb_epoch=20, batch_size=2, verbose=0)

# ---- prepare models: create a mapping of ML classifier name to algorithm
param_grid = [
    {'classifier': [model],
     'classifier__dropout_rate': [0.2],
     'feature_selection': [select],
     'feature_selection__k': [12],
     'feature_scaling': [scaler]
    }
]

# ---- Run grid search k-fold cross-validation
pcv = GridSearchCV(estimator=pipe, param_grid=param_grid, cv=3, n_jobs=-1)
pcv.fit(X_train, y_train)

y_pred = pcv.predict(X_test)
report = classification_report( y_test, y_pred )
print report

print("Pipeline steps:\n{}".format(pipe.steps))
# ---- extract the first step 
components = pipe.named_steps["feature_scaling"]
print("components: {}".format(components))
classifier = pcv.best_estimator_.named_steps["classifier"]
print("Keras DNN classifier step:\n{}".format(classifier))
print("Best cross-validation accuracy: {:.2f}".format(pcv.best_score_)) 
print("Test set score: {:.2f}".format(pcv.score(X_test, y_test))) 
print("Best parameters: {}\n".format(pcv.best_params_))


import hyperopt
from hyperopt import hp
from hyperopt.pyll.base import scope
from hyperopt import fmin, Trials, STATUS_OK, tpe
from hyperopt import space_eval

# ---- Apply the random over-sampling
#ros = RandomOverSampler()
#X_overresampled, y_overresampled = ros.fit_sample(data_X, data_y)


# ---- Data splitter
def data_split(data):
    """
    Data providing function:

    This function is separated from model() so that hyperopt
    won't reload data for each evaluation run.
    """
    X_train, X_test, y_train, y_test = train_test_split(data.drop(["y","globalTimesEventWeight"], axis=1, inplace=False).values, 
                                                        data['y'].values, test_size=0.25, 
                                                        random_state=42)

    return X_train, X_test, y_train, y_test


X_train, X_test, y_train, y_test = data_split(data)


# ---- Object functor class
class Objective:
    
    def __init__(self, X, y):
        self.X = X
        self.y = y
        self.best = 0        

    # Define the objective function
    def __call__(self, params):
        acc = self.hyperopt_train_test(params)

        if acc > self.best:
            self.best = acc
        print 'new best:', self.best, params
        return {'loss': -acc, 'status': STATUS_OK}
    
    def setEstimator(self, Classifier):
        self.Classifier = Classifier
        
    def hyperopt_train_test(self, params):
        clf = self.Classifier.set_params(**params)

        scoring = 'roc_auc'#None, 'accuracy'
        score = cross_val_score(estimator=clf, X=self.X, y=self.y, cv=3, scoring=scoring).mean()

        return score

# ---- Create model
model = KerasClassifier(build_fn=create_model, nb_epoch=150, batch_size=10, verbose=0)


# ---- Instatiate an objective function to minimize
f = Objective(X_train, y_train)
f.setEstimator(model)


from hyperopt.pyll.base import scope

# ---- Space is where you define the parameters and parameter search space
space = {
    'optimizer'     : hp.choice('optimizer', ["adam", "rmsprop"]),
    'init'          : hp.choice('init', ["uniform", "glorot_uniform"]),
    'dropout_rate'  : scope.int(hp.quniform('dropout_rate', 0., 0.2 , 0.4)),
}

# ---- Trials keeps track of all experiments
# These can be saved and loaded back into a new batch of experiments
trials = Trials()

# ---- Number of maximum evalutions (# of experiments) to run
max_evals=300

# ---- The main function to run the experiments
# The algorithm tpe.suggest runs the Tree-structured Parzen estimator
# Hyperopt does have a random search algorithm as well
# Number of maximum evalutions to run
best = fmin(fn=f, 
            space=space, 
            algo=tpe.suggest, 
            max_evals=max_evals,
            trials=trials)

print('best:', best)
